const express = require('express');

//Router() handles the requests
const router = express.Router();

//Router() handles the request
// const User = require('../models/User')

//User Controller
const userController = require('../controllers/userControllers')


const auth = require('./../auth');

//syntax: router.HTTPmethod('url', <request listener>)
// router.get('/', (req, res) => {
// 	// console.log("Hello from userRoutes")
// 	res.send("Hello from userRoutes")
// })


//with business logic
//http://localhost:3001/users
// router.post('/email-exist',(req, res) => {
// 	let email = req.body.email
// 	//find the matching document in the database using email
// 		//by using model methods
// 	User.findOne({email: email}).then((result, error) => {
// 		console.log(result)
// 		if(result != null){
// 			res.send (false)
// 		}else{
// 			if(result == null){
// 				res.send (true)
// 			}else{
// 				res.send(error)
// 			}
// 		}
// 	})

// 	//send back the response to the client
// });


//Using userControllers and without business logic
//Routes would only handle request and response
//CHECK EMAIL ROUTE
router.post('/email-exist',(req, res) => {
		//invoke the function here
		userController.checkEmail(req.body.email).then(result => res.send(result))
});



//REGISTER ROUTE
router.post('/register', (req, res)=>{
	//expecting data/object coming from request body


	userController.register(req.body).then(result => res.send(result))
})


//GET ALL USER
router.get('/', (req, res) => {

	userController.getAllUsers().then(result => res.send(result))
})


router.post('/login', (req, res) => {
	userController.login(req.body).then(result => res.send(result))
})


router.get('/details', auth.verify, (req, res) => {
	// console.log(req.headers.authorization)
	let userData = auth.decode (req.headers.authorization)
	// auth.decode(req.headers.authorization)
	userController.getUserProfile(userData).then(result => res.send(result))
})


module.exports = router;