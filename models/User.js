const mongoose = require('mongoose');


//Create a Schema
const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNo: {
		type: String,
		required:[true, "Mobile number is required"]
	},
	age: {
		type: Number,
		required: [true, "Age is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "courseId is required"]
			},
			enrolledOn: {
				type: Date, 
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		},
		{},
		{}
	]
});



//Create a model
module.exports = mongoose.model("User", userSchema);