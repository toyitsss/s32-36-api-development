
//use directive keyword require to make a module useful in the current file/module
const express = require('express')

//express function is our server stored in a constant variable
const app = express();

//require mongoose module to be used in our entry point file
const mongoose = require('mongoose');

//requiring cors
const cors = require('cors'); 

//create server port
const port = 3001;


//Express.json is an express framework to parse incoming json payloads
app.use(express.json());
app.use(express.urlencoded({ extended:true }));
//using cors
app.use(cors());


const userRoutes = require('./routes/userRoutes');


//connect to mongodb
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.2q2jw.mongodb.net/courseBooking?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});

//Notification
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log ('Connected to Database'));



//routes
	//http://localhost:3001/users
// app.get('/users', (req, res) => {
// 	res.send("Hello")
// })

// app.post('/users', (req, res) => {
// 	let name = req.body.name

// 	res.send(`Hello ${name}`)
// })


//middleware entry point url (root url before any endpoints)
app.use('/api/users', userRoutes)



//server listening to port 3001
app.listen(port, () => console.log(`Server is running at ${port}`))



