//Business logic
	//including model methods

const User = require('../models/User');

const auth = require('../auth');

const bcrypt = require ('bcrypt');

module.exports.checkEmail = (email) => {
	return	User.findOne({email: email}).then((result, error) => {
			console.log(result)

			if(result != null){
			return (false)
			}else{
				if(result == null){
					return (true)
				}else{
					return (error)
			}
		}
	})

	//send back the response to the client
}


module.exports.register = (reqBody) => {
	//save/create a new user document
		//using .save() method to save document to the database
		// console.log(reqBody)	//object user document

		//how to use object destructuring
			//why? to make distinct variables for each property w/o using dot notation
			// const {properties} = <object reference>
	const {firstName, lastName, email, password, mobileNo, age} = reqBody
	// console.log(reqBody)

	const newUser = new User({
		firstName: firstName,
		lastName: lastName,
		email: email,
		password: bcrypt.hashSync(password, 10),
		mobileNo: mobileNo,
		age: age
	})

	//save the newUser object to  the database
	return newUser.save().then( (result, error) => {
		//console.log(result)	//document
		if(result){
			return true
		} else {
			return error
		}
	})
}




module.exports.getAllUsers = () => {
	//differnce between findOne() and find()
		//findOne() returns one documents
		//find() returns an array of documents []

	return User.find({}).then(result => {
		if(result !== null){
			return result;
		}else{
			return error
		}	
	})
}


module.exports.login = (reqBody) => {
	const {email, password} = reqBody

	return User.findOne({email: email}).then((result, error) => {
		console.log(result);

		if(result == null){
			console.log('email null');
			return false;
		}else{

			let isPwdCorrect = bcrypt.compareSync(password, result.password) //boolean because it compare

			if(isPwdCorrect == true){
			//return json web token
				//invoke the function which creates the token upon logging in		
				//requirements for creating a token:
					//if password matches from existing pw from db

					return {access: auth.createAccessToken(result)}
			} else{
			return false
			}
		}
	})
}



module.exports.getUserProfile = (reqBody)=>{
	return User.findById({_id: reqBody.id}).then(result => {
		return result;
	})
}	
