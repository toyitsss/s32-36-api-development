
//json web token
	// sign - function that creates a token
	// verify - function that checks if there's presence of token
	// decode - function that decodes the token

const jwt = require('jsonwebtoken');
const secret = "okayOkayOkay"

module.exports.createAccessToken = (user) => {
	//user = object because it came from the matching documents of the user fromthe data base

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
		//jwt.sign(payload, secret, {})
	return jwt.sign(data, secret, {});
}


module.exports.decode = (token) => {
	// console.log(token)
    //jwt.decode(token, {})

    let slicedToken = token.slice(7, token.length); //para matanggal ang bearer
    // console.log(slicedToken)

    return jwt.decode(slicedToken, {complete: true}).payload
}


module.exports.verify = (req, res, next) => {

	//where to get the token?
	let token = req.headers.authorization
	// console.log(token)

	//jwt.verify(token, secret, function)

	if(typeof token !== "undefined"){
		let slicedToken = token.slice(7, token.length);
		return jwt.verify(slicedToken, secret, (err, data) =>{
			if(err){
				res.send({auth: "failed"})
			}else{
				next()
			}
		})

	}else{
		res.send(false)
	}
}